local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Intllib
local S = cooking.intllib

--
-- Pie
--
core.register_craftitem(modname..":pie_apple_unbaked", {
	description = S("Unbaked Apple Pie"),
	inventory_image = modname.."_pie_apple.png^[brighten:100 ",
	on_use = core.item_eat(2),
})

core.register_craftitem(modname..":pie_apple", {
	description = S("Apple Pie"),
	inventory_image = modname.."_pie_apple.png",
	on_use = core.item_eat(8),
})


-- Pie Recipe
local pie_recipe
if not cooking.vegan and minetest.get_modpath("creatures") and minetest.get_modpath("chicken") then
	pie_recipe = {	"default:apple", "default:apple","default:apple",
					"farming:flour", "creatures:egg","farming:flour"}
else
	pie_recipe = {	"default:apple", "default:apple","default:apple",
					"farming:flour", "group:water_bucket","farming:flour"}
end
core.register_craft({
	type = "shapeless",
	output = modname..":pie_apple_unbaked",
	recipe = pie_recipe,
	replacements = {
		{"group:water_bucket", "bucket:bucket_empty"},
	}
})
core.register_craft({
	type = "cooking",
	output = modname..":pie_apple",
	recipe = modname..":pie_apple_unbaked",
})
