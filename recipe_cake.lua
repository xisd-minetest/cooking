local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Intllib
local S = cooking.intllib

--
-- Cake
--

core.register_craftitem(modname..":cake", {
	description = S("Cake"),
	inventory_image = modname.."_cake.png",
	on_use = core.item_eat(7),
})

-- Cake Recipe
local cake_recipe
if not cooking.vegan and minetest.get_modpath("creatures") and minetest.get_modpath("chicken") then
	cake_recipe = {	"group:food_sugar", "group:food_sugar","",
			"farming:flour", "creatures:egg",""}
else
	cake_recipe = {"group:food_sugar", "group:food_sugar","",
			"farming:flour", "group:water_bucket",""}
end
core.register_craft({
	type = "shapeless",
	output = modname..":cake",
	recipe = cake_recipe,
	replacements = {
		{"group:water_bucket", "bucket:bucket_empty"},
	}
})


