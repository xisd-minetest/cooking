-- This mod add cooking recipes as well as a cooking pot
--
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for intllib.
local S, NS = dofile(modpath.."/intllib.lua")


-- Set to true to never use eggs
local SAM_IS_VEGAN = false


cooking = {}
cooking.intllib = S
cooking.vegan = SAM_IS_VEGAN


--
-- Register Sugar
--
if not minetest.get_modpath("food") then
	minetest.register_craftitem(modname..":sugar", {
		description = S("Sugar"),
		inventory_image = modname.."_sugar.png",
		groups = {food_sugar=1}
	})
	
	minetest.register_craft({
		type = "shapeless",
		output = modname..":sugar",
		recipe = {"default:papyrus"}
	})
else
	minetest.register_alias(modname..":sugar", "food:sugar")
end

--dofile(modpath.."/cooking_pot.lua")
dofile(modpath.."/recipe_cake.lua")
dofile(modpath.."/recipe_pie_apple.lua")
